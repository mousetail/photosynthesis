from collections import defaultdict
from statistics import stdev
from itertools import combinations, permutations
import typing
import collections


def _iter_pairs(p2: list[set[frozenset[int]]]):
    for p in p2:
        if p:
            yield next(iter(p))


def custom_matchmaker(
    nrof_players: int, game_size: int, nrof_games_per_pair: int = 1
) -> typing.Generator[tuple[int, int, int, int], None, None]:
    missing_pairs: list[set[frozenset[int]]] = [
        {frozenset(i) for i in combinations(range(nrof_players), 3)}
        for i in range(nrof_games_per_pair)
    ]
    nrof_games_per_player: collections.Counter[int] = collections.Counter()

    while missing_pairs:
        game: set[int] = set()

        missing_pairs_iter = _iter_pairs(missing_pairs)
        game |= next(missing_pairs_iter)

        while len(game) < game_size:
            game.add(
                min(
                    range(nrof_players),
                    key=lambda i: (i in game, nrof_games_per_player[i]),
                )
            )
        else:  # check if too many players have been added to the game
            while len(game) > game_size:
                removed_player = game.pop()

        for pair in combinations(game, 3):
            pair_set = frozenset(pair)
            for missing_pair_set in missing_pairs:
                if pair_set in missing_pair_set:
                    missing_pair_set.remove(pair_set)
                    break

        k = typing.cast(tuple[int, int, int, int], tuple(game))
        for i in k:
            nrof_games_per_player[i] += 1

        print("boo")
        print("pairs", ", ".join(str(len(i)) for i in missing_pairs))
        yield k
        yield (k[1], k[3], k[0], k[2])

        missing_pairs = [i for i in missing_pairs if len(i) > 0]
