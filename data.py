import dataclasses
import enum
import typing
import pygame
import random

GRID_SIZE = 15


@dataclasses.dataclass
class Tile:
    owner: typing.Optional[int] = None
    stack: int = 0

    def rotate(self):
        if self.owner is None or self.owner == 4:
            return Tile(owner=self.owner, stack=self.stack)
        else:
            return Tile(owner=(self.owner - 1 + 4) % 4, stack=self.stack)

    def __bool__(self):
        return self.owner is not None


@dataclasses.dataclass(slots=True, frozen=True)
class Grid:
    tiles: list[list[Tile]]
    turns: int = 0

    def rotate(self) -> "Grid":
        return Grid(
            tiles=[
                [self.tiles[GRID_SIZE - 1 - i][j].rotate() for j in range(GRID_SIZE)]
                for i in range(GRID_SIZE)
            ],
            turns=self.turns + 1,
        )

    def __post_init__(self):
        assert len(self.tiles) == GRID_SIZE
        assert len(self.tiles[0]) == GRID_SIZE

    @staticmethod
    def new() -> "Grid":
        tiles = [
            [Tile(owner=None, stack=0) for i in range(GRID_SIZE)]
            for j in range(GRID_SIZE)
        ]
        tiles[0][0] = Tile(owner=0, stack=1)
        tiles[GRID_SIZE - 1][0] = Tile(owner=3, stack=1)
        tiles[GRID_SIZE - 1][GRID_SIZE - 1] = Tile(owner=2, stack=1)
        tiles[0][GRID_SIZE - 1] = Tile(owner=1, stack=1)

        for i in range(5):
            block_x = random.randrange(1, GRID_SIZE // 2)
            block_y = random.randrange(1, GRID_SIZE // 2)

            for i in range(4):
                tiles[block_x][block_y] = Tile(owner=4, stack=0)
                block_x, block_y = block_y, GRID_SIZE - block_x - 1

        return Grid(tiles=tiles)

    def __iter__(self) -> typing.Generator[tuple[int, int], None, None]:
        for i in range(GRID_SIZE):
            for j in range(GRID_SIZE):
                yield i, j

    def __getitem__(self, value: tuple[int, int]) -> Tile:
        if (
            value[0] < 0
            or value[0] >= GRID_SIZE
            or value[1] < 0
            or value[1] >= GRID_SIZE
        ):
            return Tile(owner=-1)
        return self.tiles[value[0]][value[1]]

    def get_score(self, color: int) -> int:
        count = 0
        for i in self:
            count += self[i].owner == color
        return count


@enum.unique
class ActionType(enum.Enum):
    stay = "Stay"
    move = "Move"
    split = "Split"


@dataclasses.dataclass
class Action:
    source: tuple[int, int]
    action_type: ActionType
    destination: typing.Optional[tuple[int, int]] = None


@dataclasses.dataclass
class Score:
    first: int = 0
    second: int = 0
    third: int = 0
    fourth: int = 0

    def get_score(self) -> int:
        return (
            8
            * 3
            * 5
            * 7
            * (self.first * 3 + self.second * 2 + self.third * 1)
            // (self.first + self.second + self.third + self.fourth)
        )

    def total_games(self) -> int:
        return self.first + self.second + self.third + self.fourth

    def to_json(self) -> dict[str, typing.Any]:
        return {
            "first": self.first,
            "second": self.second,
            "third": self.third,
            "fourth": self.fourth,
        }

    @staticmethod
    def from_json(data: dict[str, typing.Any]):
        return Score(
            first=data.get("first", 0),
            second=data.get("second", 0),
            third=data.get("third", 0),
            fourth=data.get("fourth", 0),
        )


@dataclasses.dataclass
class GameLogEntry:
    participants: set[int]
    scores: dict[int, int]

    def to_json(self) -> dict[str, typing.Any]:
        return {
            "participants": list(self.participants),
            "scores": [
                {"player": key, "score": value} for key, value in self.scores.items()
            ],
        }

    @staticmethod
    def from_json(data: dict[str, typing.Any]):
        return GameLogEntry(
            participants=set(typing.cast(list[int], data.get("participants"))),
            scores={
                int(i.get("player", 0)): typing.cast(int, i.get("score"))
                for i in typing.cast(list[dict], data.get("scores"))
            },
        )


@dataclasses.dataclass
class Competitor:
    callable: typing.Callable[[Grid], list[Action]]
    image: pygame.surface.Surface

    def to_json(self) -> dict[str, typing.Any]:
        return {"name": self.callable.__name__, "docs": self.callable.__doc__}


@dataclasses.dataclass
class SerializedCompetitor:
    name: str
    docs: str

    @staticmethod
    def from_json(data: dict[str, typing.Any]) -> "SerializedCompetitor":
        return SerializedCompetitor(
            name=str(data.get("name")), docs=str(data.get("docs"))
        )


@dataclasses.dataclass
class RankingState:
    scores: dict[int, Score]
    bots: tuple[int, int, int, int]
    competitors: list[Competitor]
    grid: Grid
    game_log: list[GameLogEntry]

    def to_json(self) -> dict[str, typing.Any]:
        return {
            "scores": [
                {"id": key, "score": score.to_json()}
                for key, score in self.scores.items()
            ],
            "bots": self.bots,
            "competitors": [i.to_json() for i in self.competitors],
            "game_log": [i.to_json() for i in self.game_log],
        }


@dataclasses.dataclass
class SerializedRankingState:
    scores: dict[int, Score]
    bots: tuple[int, int, int, int]
    competitors: list[SerializedCompetitor]
    game_log: list[GameLogEntry]

    @staticmethod
    def from_json(data: dict[str, typing.Any]) -> "SerializedRankingState":
        return SerializedRankingState(
            scores={
                i.get("id", 0): Score.from_json(i.get("score"))
                for i in typing.cast(list, data.get("scores"))
            },
            bots=typing.cast(tuple[int, int, int, int], data.get("bots")),
            competitors=[
                SerializedCompetitor.from_json(i)
                for i in typing.cast(list, data.get("competitors"))
            ],
            game_log=[GameLogEntry.from_json(i) for i in data.get("game_log", {})],
        )
