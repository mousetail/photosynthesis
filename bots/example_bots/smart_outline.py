import data
import typing


def outline_bot(grid) -> list[data.Action]:
    """Builds a "Wall" to prevent others from coming in"""

    def is_row_filled(row):
        for i in range(0, row + 1):
            if not grid[0, i] or not grid[i, 0]:
                return False
        return True

    def neighbors(t: tuple[int, int]):
        yield (t[0] + 1, t[1])
        yield (t[0], t[1] + 1)
        yield (t[0] - 1, t[1])
        yield (t[0], t[1] - 1)

    def cell_towards(
        cell: tuple[int, int], next_cell: tuple[int, int]
    ) -> tuple[int, int]:
        diff = (cell[0] - next_cell[0], cell[1] - next_cell[1])
        if diff == (0, 0):
            diff = (0, 1)

        axis = max((0, 1), key=lambda i: abs(diff[i]))
        return typing.cast(
            tuple[int, int],
            tuple(
                (cell[j] - (i // abs(i) if axis == j else 0))
                for j, i in enumerate(diff)
            ),
        )

    def get_cell_outwards(cell: tuple[int, int]) -> tuple[int, int]:
        if cell[1] > cell[0]:
            return (cell[0], cell[1] + 1)
        else:
            return (cell[0] + 1, cell[1])

    def get_cell_behind(cell: tuple[int, int]):
        if cell[1] > cell[0]:
            return (cell[0], cell[1] - 1)
        else:
            return (cell[0] - 1, cell[1])

    visited: set[tuple[int, int]] = set()
    actions: list[data.Action] = []

    # print("\n === TURN ===")

    my_tiles = [i for i in grid if grid[i].owner == 0]
    my_tiles.sort(key=lambda t: t[0] + t[1], reverse=True)

    for cell in my_tiles:
        corner = (max(cell), max(cell))
        cell_towards_corner = cell_towards(cell, corner)
        cell_outwards = get_cell_outwards(cell)
        cell_behind = get_cell_behind(cell)
        if (
            grid[cell_towards_corner].owner is None
            and cell_towards_corner not in visited
            and cell[0] != cell[1]
            and grid[cell_behind].owner is not None
        ):
            actions.append(
                data.Action(
                    action_type=data.ActionType.move,
                    source=cell,
                    destination=cell_towards_corner,
                )
            )
            visited.add(cell_towards_corner)
            # print(f"{cell} moving towards corner {cell_towards_corner}")
        elif (
            is_row_filled(max(cell))
            and grid[cell_outwards].owner is None
            and cell_outwards not in visited
            and cell != (0, 0)
            and (cell[0] != cell[1] or is_row_filled(cell[0]))
        ):
            actions.append(
                data.Action(
                    action_type=data.ActionType.move,
                    source=cell,
                    destination=cell_outwards,
                )
            )
            visited.add(cell_outwards)
            # print(
            #    f"{cell} moving towards edge {cell_outwards} ({cell_towards_corner=}) ({is_row_filled(max(cell))=})"
            # )
        elif grid[cell].stack <= 1:
            actions.append(data.Action(action_type=data.ActionType.stay, source=cell))

        elif grid[cell_outwards].owner is None and cell_outwards not in visited:
            actions.append(
                data.Action(
                    action_type=data.ActionType.split,
                    source=cell,
                    destination=cell_outwards,
                )
            )
            visited.add(cell_outwards)

            # print(f"{cell} splitting towards edge {cell_outwards}")
        else:
            for neighbor in neighbors(cell):
                if neighbor not in visited and grid[neighbor].owner is None:
                    actions.append(
                        data.Action(
                            action_type=data.ActionType.split,
                            source=cell,
                            destination=neighbor,
                        )
                    )
                    visited.add(neighbor)
                    break
            else:
                actions.append(
                    data.Action(action_type=data.ActionType.stay, source=cell)
                )

    return actions
