import data


def expand_bot(grid) -> list[data.Action]:
    """Just splits"""

    def neighbors(t: tuple[int, int]):
        yield (t[0] + 1, t[1])
        yield (t[0], t[1] + 1)
        yield (t[0] - 1, t[1])
        yield (t[0], t[1] - 1)

    actions = []
    reached: set[tuple[int, int]] = set()
    for cell in grid:
        if grid[cell].owner == 0:
            if grid[cell].stack < 2:
                actions.append(
                    data.Action(action_type=data.ActionType.stay, source=cell)
                )
            else:
                try:
                    destination = next(
                        i
                        for i in neighbors(cell)
                        if grid[i].owner is None and i not in reached
                    )
                except StopIteration:
                    actions.append(
                        data.Action(action_type=data.ActionType.stay, source=cell)
                    )
                else:
                    reached.add(destination)
                    actions.append(
                        data.Action(
                            action_type=data.ActionType.split,
                            source=cell,
                            destination=destination,
                        )
                    )
    return actions
