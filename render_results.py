from email.generator import Generator
import json
import typing
import data
from html import escape
import statistics


def format_name(name):
    return name.replace("_", " ").title()


def score_diffs(
    state: data.SerializedRankingState, bot: int
) -> typing.Generator[tuple[data.SerializedCompetitor, float, int], None, None]:
    global_average = statistics.mean(
        game.scores[bot] for game in state.game_log if bot in game.participants
    )

    for index, player in enumerate(state.competitors):
        avg_score = (
            statistics.mean(
                game.scores[bot]
                for game in state.game_log
                if bot in game.participants and index in game.participants
            )
            - global_average
        )
        nrof_games = sum(
            1
            for game in state.game_log
            if bot in game.participants and index in game.participants
        )
        yield player, avg_score, nrof_games


def render_results(state: data.SerializedRankingState):
    with open("output.html", "w") as f:
        f.write(
            """
        <html>
        <head>
        <title>Photosynthesis</title>
        <link rel="stylesheet" href="style.css">
        </head>
        <body>

        <h1>Scores</h1>

        <table class="normal bot-info-table">
        <colgroup>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
        </colgroup>
        <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Median / Mean Score</th>
                <th>BFF</th>
                <th>Nemisis</th>
                <th>Total Games</th>
                <th>Placement</th>
                <th>Score</th>
            </tr>
            </thead>
        <tbody>
        """
        )

        for index, bot in enumerate(
            sorted(state.scores, key=lambda i: -state.scores[i].get_score())
        ):
            bff = max(score_diffs(state, bot), key=lambda i: i[1])
            nemisis = min(score_diffs(state, bot), key=lambda i: i[1])

            f.write(
                f"""
                <tr>
                    <td>#{index+1} <img src="{escape("bot_images_cache/"+state.competitors[bot].name+".png")}"></td>
                    <td><b>{escape(format_name(state.competitors[bot].name))}</b><br>
                        <i>{escape(state.competitors[bot].docs)}</i>
                    </td>
                    <td>{statistics.median(game.scores[bot] for game in state.game_log if bot in game.participants)} /
                        {statistics.mean(game.scores[bot] for game in state.game_log if bot in game.participants):.1f}
                    </td>
                    <td>
                        <i><img src="{escape("bot_images_cache/"+bff[0].name+".png")}" class="small"> {escape(format_name(bff[0].name))}</i><br>
                        <span>{bff[1]:+.1f} (N={bff[2]})</span>
                    </td>
                    <td>
                        <i><img src="{escape("bot_images_cache/"+nemisis[0].name+".png")}" class="small"> {escape(format_name(nemisis[0].name))}</i><br>
                        <span>{nemisis[1]:+.1f} (N={bff[2]})</span>
                    </td>
                    <td>{state.scores[bot].total_games()}</td>
                    <td>
                        <div class="progressbar">
                            <div style="flex-grow: {10 * state.scores[bot].first / state.scores[bot].total_games():.2f}">1st</div>
                            <div style="flex-grow: {10 * state.scores[bot].second / state.scores[bot].total_games():.2f}">2nd</div>
                            <div style="flex-grow: {10 * state.scores[bot].third / state.scores[bot].total_games():.2f}">3rd</div>
                            <div style="flex-grow: {10 * state.scores[bot].fourth / state.scores[bot].total_games():.2f}">4th</div>
                        </div>
                    </td>
                    <td>{state.scores[bot].get_score():,}</td>
                </tr>
                """
            )
        f.write("</tbody></table>")
        f.write("<h1>Game Log</h1>")
        f.write('<table class="double game-log">')
        f.write(
            """
            <colgroup>
                <col>
                <col>
                <col>
                <col>
                <col>
                <col>
                <col>
                <col>
            </colgroup>
            <tbody>
        """
        )

        for index, game in enumerate(state.game_log):
            f.write("<tr>")
            f.write(f"<th rowspan='2'>{escape(str(index))}</th>")
            for participant in sorted(
                game.participants, key=lambda i: game.scores[i], reverse=True
            ):
                f.write(
                    f'<td rowspan="2"><img src="{escape("bot_images_cache/"+state.competitors[participant].name+".png")}"></td>'
                )
                f.write("<th>")
                f.write(escape(format_name(state.competitors[participant].name)))
                f.write("</th>\n")
            f.write("</tr><tr>")
            for participant in sorted(
                game.participants, key=lambda i: game.scores[i], reverse=True
            ):
                f.write("<td>")
                f.write(escape(str(game.scores[participant])))
                f.write("</td>\n")
            f.write("</tr>")
        f.write("</tbody></table>")
        f.write("</body></html>")


if __name__ == "__main__":
    with open("screenshots/result.json", "r", encoding="utf-8") as f:
        render_results(data.SerializedRankingState.from_json(json.load(f)))
