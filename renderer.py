import json
import controller
import pygame
import data
import typing
import requests
import render_results
import io
import os
import random
import match_generator
import hashlib
from competitors import competitors


colors = [(255, 50, 50), (25, 220, 25), (50, 50, 255), (205, 205, 0), (0, 0, 0)]

MARGIN_SIZE = 64
random.seed("Photoythesis is coool!1!111!!!!")


def draw_grid(screen: pygame.surface.Surface, grid: data.Grid):
    for (x, y) in grid:
        owner = grid[x, y].owner
        if owner is not None:
            pos = (
                int(x * 512 / data.GRID_SIZE + MARGIN_SIZE + 2),
                int(y * 512 / data.GRID_SIZE + MARGIN_SIZE + 2),
                int(512 / data.GRID_SIZE - 4),
                int(512 / data.GRID_SIZE - 4),
            )
            screen.fill(colors[owner], pos)
            t = font.render(str(grid[x, y].stack), True, (255, 255, 255))
            screen.blit(
                t,
                (
                    pos[0] + pos[2] // 2 - t.get_width() // 2,
                    pos[1] + pos[3] // 2 - t.get_height() // 2,
                ),
            )


def draw_bot_labels(screen: pygame.surface.Surface, state: data.RankingState):
    for index, bot in enumerate(state.bots):
        text = font.render(state.competitors[bot].callable.__name__, True, 0)
        score_text = font.render(
            str(state.grid.get_score(index)), True, (255, 255, 255)
        )
        bot_score = state.scores[bot]
        total_score_text = font.render(
            f"{bot_score.first} / {bot_score.second} / {bot_score.third} / {bot_score.fourth}",
            True,
            colors[index],
        )

        color_pos: complex = -256 + MARGIN_SIZE // 4
        text_pos: complex = -256 + MARGIN_SIZE // 2 + text.get_width() // 2 + 10
        score_pos: complex = (
            256
            - MARGIN_SIZE
            - score_text.get_width() // 2
            - total_score_text.get_width()
        )
        score_top_left: complex = (
            256
            - MARGIN_SIZE
            - score_text.get_width()
            - 10
            - MARGIN_SIZE // 4 * 1j
            - total_score_text.get_width()
        )
        score_bottom_right: complex = (
            256
            - MARGIN_SIZE
            + 10
            + MARGIN_SIZE // 4 * 1j
            - total_score_text.get_width()
        )
        total_score_pos: complex = 256 - total_score_text.get_width() // 2

        text = pygame.transform.rotate(text, 90 * index)
        score_text = pygame.transform.rotate(score_text, 90 * index)
        total_score_text = pygame.transform.rotate(total_score_text, 90 * index)
        bot_image = pygame.transform.rotate(bot_avatars[bot], 90 * index)

        (
            color_pos,
            text_pos,
            score_pos,
            score_top_left,
            score_bottom_right,
            total_score_pos,
        ) = (
            (i + (-256 - MARGIN_SIZE // 2) * 1j) * (-1j) ** (index)
            + (256 + MARGIN_SIZE)
            + (256 + MARGIN_SIZE) * 1j
            for i in (
                color_pos,
                text_pos,
                score_pos,
                score_top_left,
                score_bottom_right,
                total_score_pos,
            )
        )

        pygame.gfxdraw.circle(
            screen,
            int(color_pos.real),
            int(color_pos.imag),
            MARGIN_SIZE // 4,
            colors[index],
        )

        pygame.gfxdraw.filled_circle(
            screen,
            int(color_pos.real),
            int(color_pos.imag),
            MARGIN_SIZE // 4,
            colors[index],
        )

        screen.blit(
            bot_image,
            (
                int(color_pos.real) - bot_avatars[bot].get_width() // 2,
                int(color_pos.imag) - bot_avatars[bot].get_height() // 2,
            ),
        )

        pygame.gfxdraw.filled_circle(
            screen,
            int(score_pos.real),
            int(score_pos.imag),
            MARGIN_SIZE // 6,
            (0, 0, 0),
        )

        pygame.draw.polygon(
            screen,
            colors[index],
            (
                (score_top_left.real, score_top_left.imag),
                (score_bottom_right.real, score_top_left.imag),
                (score_bottom_right.real, score_bottom_right.imag),
                (score_top_left.real, score_bottom_right.imag),
            ),
            0,
        )

        screen.blit(
            text,
            (
                text_pos.real - text.get_width() // 2,
                text_pos.imag - text.get_height() // 2,
            ),
        )
        screen.blit(
            score_text,
            (
                score_pos.real - score_text.get_width() // 2,
                score_pos.imag - score_text.get_height() // 2,
            ),
        )
        screen.blit(
            total_score_text,
            (
                total_score_pos.real - total_score_text.get_width() // 2,
                total_score_pos.imag - total_score_text.get_height() // 2,
            ),
        )


def load_image(competitor):
    dicebear_categories = [
        "adventurer",
        "adventurer-neutral",
        "avataaars",
        "big-ears",
        "big-ears-neutral",
        "big-smile",
        "bottts",
        "croodles",
        "croodles-neutral",
        "identicon",
        "initials",
        "micah",
        "miniavs",
        "open-peeps",
        "personas",
        "pixel-art",
        "pixel-art-neutral",
    ]

    if not os.path.isdir("bot_images_cache"):
        os.mkdir("bot_images_cache")

    if os.path.exists("bot_images_cache/" + competitor.__name__ + ".png"):
        return pygame.transform.scale(
            pygame.image.load(
                "bot_images_cache/" + competitor.__name__ + ".png"
            ).convert_alpha(),
            (int(MARGIN_SIZE * 0.8), int(MARGIN_SIZE * 0.8)),
        )
    else:
        hsh = hashlib.sha224(competitor.__name__.encode("utf-8")).digest()[-1] % (
            4 + len(dicebear_categories)
        )
        if hsh >= len(dicebear_categories):
            res = requests.get(
                "https://robohash.org/b-" + competitor.__name__ + ".png",
                params={
                    "size": f"{int(MARGIN_SIZE * 0.8)}x{int(MARGIN_SIZE * 0.8)}",
                    "set": f"set{hsh-11}",
                },
                headers={"Accept": "Image/PNG"},
            )
        else:
            category = dicebear_categories[hsh]

            res = requests.get(
                f"https://avatars.dicebear.com/api/{category}/{competitor.__name__}.png",
                headers={"Accept": "Image/PNG"},
            )
        res.raise_for_status()

        img = pygame.image.load(io.BytesIO(res.content), ".png").convert_alpha()

        with open("bot_images_cache/" + competitor.__name__ + ".png", "wb") as f:
            f.write(res.content)

        return pygame.transform.scale(
            img, (int(MARGIN_SIZE * 0.8), int(MARGIN_SIZE * 0.8))
        )


if __name__ == "__main__":
    screen = pygame.display.set_mode((512 + MARGIN_SIZE * 2, 512 + MARGIN_SIZE * 2))

    bot_avatars = [load_image(i) for i in competitors]

    matchup_generator = match_generator.custom_matchmaker(
        len(competitors), 4, nrof_games_per_pair=1
    )  # Last attempt: 57 games

    state = data.RankingState(
        grid=data.Grid.new(),
        bots=next(matchup_generator),
        scores={i: data.Score() for i in range(len(competitors))},
        competitors=[
            data.Competitor(callable=i, image=j)
            for i, j in zip(competitors, bot_avatars)
        ],
        game_log=[],
    )

    running = True
    frame_nr = 0

    pygame.font.init()
    font = pygame.font.Font(None, 25)

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        screen.fill((255, 255, 255))

        draw_grid(screen, state.grid)
        draw_bot_labels(screen, state)

        frame_nr += 1
        if frame_nr % 8 == 0 or pygame.key.get_pressed()[pygame.K_SPACE]:
            for bot in state.bots:
                actions = state.competitors[bot].callable(state.grid)
                if controller.validate_actions(state.grid, actions, 0):
                    state.grid = controller.apply_actions(state.grid, actions, 0)
                else:
                    print(f"Invalid action by {competitors[bot].__name__}")
                state.grid = state.grid.rotate()

            last_game_scores = tuple(state.grid.get_score(i) for i in range(4))
            total_score = sum(last_game_scores) + state.grid.get_score(4)
            if (
                total_score == data.GRID_SIZE * data.GRID_SIZE
                or state.grid.turns > data.GRID_SIZE * data.GRID_SIZE * 4
            ):
                print(state.grid.turns)
                log_entry = data.GameLogEntry(
                    participants=set(state.bots),
                    scores={state.bots[i]: last_game_scores[i] for i in range(4)},
                )
                state.game_log.append(log_entry)

                # sorted_scores = sorted(last_game_scores)
                placement = {
                    index: newindex
                    for newindex, (index, score) in enumerate(
                        sorted(enumerate(last_game_scores), key=lambda i: i[1])
                    )
                }

                state.scores[state.bots[placement[3]]].first += 1
                state.scores[state.bots[placement[2]]].second += 1
                state.scores[state.bots[placement[1]]].third += 1
                state.scores[state.bots[placement[0]]].fourth += 1
                try:
                    state.bots = next(matchup_generator)
                except StopIteration:
                    serialized_data = state.to_json()
                    with open("screenshots/result.json", "w") as f:
                        json.dump(serialized_data, f)

                    render_results.render_results(
                        data.SerializedRankingState.from_json(serialized_data)
                    )
                    running = False
                state.grid = data.Grid.new()
        pygame.display.flip()

        if not pygame.key.get_pressed()[pygame.K_SPACE]:
            if frame_nr % 8 == 0:
                pygame.image.save(screen, f"screenshots/{frame_nr}.png")
            pygame.time.wait(75)
